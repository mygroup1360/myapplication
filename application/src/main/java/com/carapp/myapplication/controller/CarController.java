package com.carapp.myapplication.controller;

import com.carapp.myapplication.exception.CarNotFoundException;
import com.carapp.myapplication.model.Car;
import com.carapp.myapplication.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("cars")
@RequiredArgsConstructor
public class CarController {
    private final CarService carService;

    @GetMapping
    public List<Car> getAllCars() {
        return carService.getAllCars();
    }

    @GetMapping("{id}")
    public Car getCarById(@PathVariable String id) throws CarNotFoundException {
        return carService.getCarById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String createCar(@RequestBody Car car) {
        return carService.createCar(car);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCar(@RequestBody Car car) throws CarNotFoundException {
        carService.updateCar(car);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCar(@PathVariable String id) throws CarNotFoundException {
        carService.removeCarById(id);
    }

}
