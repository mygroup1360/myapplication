package com.carapp.myapplication.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandlerException {
    private static final Logger logger = LoggerFactory.getLogger("exceptionHandler");

    @ExceptionHandler
    public ResponseEntity<String> exception(ObjectNotFoundException exception) {
        logger.error("ErrorHandler. BadRequest", exception);
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }
}
