package com.carapp.myapplication.exception;

public class CarNotFoundException extends ObjectNotFoundException {
    public CarNotFoundException(String message) {
        super(message);
    }
}
