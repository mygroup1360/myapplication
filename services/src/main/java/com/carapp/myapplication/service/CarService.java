package com.carapp.myapplication.service;

import com.carapp.myapplication.exception.CarNotFoundException;
import com.carapp.myapplication.model.Car;

import java.util.List;

public interface CarService {
    List<Car> getAllCars();

    String createCar(Car car);

    Car getCarById(String id) throws CarNotFoundException;

    void updateCar(Car car) throws CarNotFoundException;

    void removeCarById(String id) throws CarNotFoundException;

}
