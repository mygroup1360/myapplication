package com.carapp.myapplication.service.impl;

import com.carapp.myapplication.exception.CarNotFoundException;
import com.carapp.myapplication.model.Car;
import com.carapp.myapplication.service.CarService;
import com.carapp.myapplication.storage.CarRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
//import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;

    @Override
    public List<Car> getAllCars() {
        return carRepository.findAll();
    }

    @Override
    public String createCar(Car car) {
        car.setId(null);
        carRepository.save(car);
        return car.getId();
    }

    @Override
    public Car getCarById(String id) throws CarNotFoundException {
        return carRepository.findById(id)
                .orElseThrow(() -> new CarNotFoundException("Car not found with an id: " + id));
    }

    @Override
    public void updateCar(Car car) throws CarNotFoundException {
        if (car.getId() == null) {
            throw new CarNotFoundException("Id of the car not given");
        }
        this.getCarById(car.getId());
        carRepository.save(car);
    }

    @Override
    public void removeCarById(String id) throws CarNotFoundException {
        this.getCarById(id);
        carRepository.deleteById(id);
    }
}
