package com.carapp.myapplication.storage;

import com.carapp.myapplication.model.Car;

import java.util.List;
import java.util.Optional;

public interface CarRepository {
    List<Car> findAll();

    Optional<Car> findById(String id);

    void save(Car car);

    void deleteById(String id);
}
