package com.carapp.myapplication.dao;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Data
@Log4j2
@Component
@ConfigurationProperties(prefix = "com.carapp.myapplication")
public class DatabaseProperties {
    private String uri;
    private String databaseName;

    @PostConstruct
    void init() {
        log.info("Opening database at " + uri + " with name " + databaseName);
    }
}
