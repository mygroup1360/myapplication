package com.carapp.myapplication.dao;

import com.carapp.myapplication.model.Car;
import com.carapp.myapplication.storage.CarRepository;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;

@Repository
public class MongoCarDAO implements CarRepository {
    private final MongoCollection<Document> collection;

    public MongoCarDAO(DatabaseProperties properties) {
        MongoClient mongoClient = new MongoClient(new MongoClientURI(properties.getUri()));
        MongoDatabase database = mongoClient.getDatabase(properties.getDatabaseName());
        this.collection = database.getCollection("car");
    }

    @Override
    public List<Car> findAll() {
        List<Car> carList = new ArrayList<>();
        FindIterable<Document> iterable = collection.find();
        for (Document document : iterable) {
            Car car = getCarFromDocument(document);
            carList.add(car);
        }
        return carList;
    }

    @Override
    public Optional<Car> findById(String id) {
        Document document;
        try {
            FindIterable<Document> iterable = collection.find(eq("_id", new ObjectId(id)));
            document = iterable.first();
        } catch (IllegalArgumentException ex) {
            return Optional.empty();
        }
        if (document == null) {
            return Optional.empty();
        } else {
            return Optional.of(getCarFromDocument(document));
        }
    }

    @Override
    public void deleteById(String id) {
        collection.deleteOne(eq("_id", new ObjectId(id)));
    }

    @Override
    public void save(Car car) {
        Document document = new Document();
        document.append("brand", car.getBrand());
        document.append("name", car.getName());
        document.append("color", car.getColor());
        document.append("year", car.getYear());
        if (car.getId() != null) {
            document.append("_id", new ObjectId(car.getId()));
            collection.updateOne(eq("_id", document.get("_id", ObjectId.class)), new Document("$set", document));
        } else {
            document.append("_id", new ObjectId());
            collection.insertOne(document);
            car.setId(document.getObjectId("_id").toString());
        }
    }

    private Car getCarFromDocument(Document document) {
        Car car = new Car();
        car.setId(document.get("_id", ObjectId.class).toString());
        car.setBrand(document.get("brand", String.class));
        car.setName(document.get("name", String.class));
        car.setColor(document.get("color", String.class));
        car.setYear(document.get("year", Integer.class));
        return car;
    }
}
